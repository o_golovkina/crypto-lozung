﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Slogan
{
    class SloganCipher
    {
        char[] table;

        public SloganCipher(string slogan)
        {
            if (slogan == null) throw new ArgumentNullException("slogan");

            this.Slogan = slogan;
            CreateTable();
        }

        public string Slogan
        {
            get;
            private set;
        }

        public string Cipher(string text)
        {
            if (string.IsNullOrEmpty(text)) return string.Empty;

            var buffer = new StringBuilder(text.Length);
            foreach (var letter in text)
                buffer.Append(IsRussianLetter(letter) ? table[(int)(letter - 'а')] : letter);
            return buffer.ToString();
        }

        public string Decipher(string text)
        {
            if (string.IsNullOrEmpty(text)) return string.Empty;

            var buffer = new StringBuilder(text.Length);
            foreach (var letter in text)
                buffer.Append(IsRussianLetter(letter) ? (char)('а' + Array.IndexOf(table, letter)) : letter);
            return buffer.ToString();
        }

        private void CreateTable()
        {
            table = Slogan.Where(IsRussianLetter).Distinct().Concat("абвгдежзиклмнопрстуфхцчшщыьъэюя".Except(Slogan)).ToArray();
        }
        private bool IsRussianLetter(char character)
        {
            return char.IsLetter(character) && character >= 'а' && character <= 'я';
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Лозунг: ");
            var slogan = Console.ReadLine();

            var cipher = new SloganCipher(slogan);

            Console.Write("Текст для шифрования: ");
            var text = Console.ReadLine();

            var cipheredText = cipher.Cipher(text);
            Console.WriteLine("Зашифрованный текст: {0}", cipheredText);

            var decipheredText = cipher.Decipher(cipheredText);
            Console.WriteLine("Расшифрованный текст: {0}", decipheredText);

            Console.ReadLine();
        }
    }
}
